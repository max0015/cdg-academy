def ask_user(question, output=STDOUT, input=STDIN)
    loop do
        output.puts question
        response = input.gets.chomp
        return response unless response.empty?
    end
end

def greeting(output=STDOUT, input=STDIN)
    name = ask_user("Your name:", output, input)
    surname = ask_user("Your surname:", output, input)
    age = ask_user("Your age:", output, input).to_i

    if age < 18
        output.puts "Привет, #{name} #{surname}, тебе меньше 18 лет, но начать программировать никогда не поздно"
    else
        output.puts "Привет, #{name} #{surname}. Самое время заняться делом!"
    end
end
