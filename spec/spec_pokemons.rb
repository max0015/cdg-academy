require 'rspec'
require 'stringio'

require_relative '../pokemons.rb'

RSpec.describe 'pokemons' do

    name = "Pikachu"
    color = "Yellow"

    name2 = "Bulbasaur"
    color2 = "Green"

    it "test get_pokemon" do
        input = StringIO.new("#{name}\n#{color}\n")
        output = StringIO.new()

        expect(output).to receive(:puts).with("Enter pokemon`s name:")
        expect(output).to receive(:puts).with("Enter pokemon`s color:")        
        
        expect(get_pokemon(input, output)).to eq({
            name: name,
            color: color
        })
    end

    it "test pokemons" do
        input = StringIO.new(<<~TEXT
            2
            #{name}
            #{color}
            #{name2}
            #{color2}
            TEXT
        )

        output = StringIO.new()

        expect(output).to receive(:puts).with("How many pokemons you wish to add?")

        2.times do
            expect(output).to receive(:puts).with("Enter pokemon`s name:")
            expect(output).to receive(:puts).with("Enter pokemon`s color:")
        end

        expect(pokemons(input, output)).to eq([
            { name: name, color: color },
            { name: name2, color: color2 },
        ])
    end
end