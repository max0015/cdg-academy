require 'rspec'
require_relative '../foobar.rb'


RSpec.describe 'foobar' do
	it "return summ of numbers" do
		expect(foobar(1, 2)).to eq(3)
	end

	it "return second number 20 is first" do
		expect(foobar(20, 2)).to eq(2)
	end

	it "return second number 20 is second" do
		expect(foobar(1, 20)).to eq(20)
	end

	it "wrong result" do
		expect(foobar(20, 20)).not_to eq(40)
	end
end