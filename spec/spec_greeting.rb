require 'rspec'
require 'stringio'

require_relative '../greeting.rb'

RSpec.describe 'greeting' do

    it "18 y.o. person" do
    	input = StringIO.new("John\nDoe\n18\n")
        output = StringIO.new()

        expect(output).to receive(:puts).with("Your name:")
        expect(output).to receive(:puts).with("Your surname:")
        expect(output).to receive(:puts).with("Your age:")
        expect(output).to receive(:puts).with("Привет, John Doe. Самое время заняться делом!")
        greeting(output, input)
    end

    it "15 y.o. person" do
    	input = StringIO.new("John\nSmith\n15\n")
        output = StringIO.new()

        expect(output).to receive(:puts).with("Your name:")
        expect(output).to receive(:puts).with("Your surname:")
        expect(output).to receive(:puts).with("Your age:")
        expect(output).to receive(:puts).with("Привет, John Smith, тебе меньше 18 лет, но начать программировать никогда не поздно")
        greeting(output, input)
    end

    it "empty name" do
        input = StringIO.new("\nJohn\nSmith\n15\n")
        output = StringIO.new()

        expect(output).to receive(:puts).with("Your name:") # empty
        expect(output).to receive(:puts).with("Your name:")
        expect(output).to receive(:puts).with("Your surname:")
        expect(output).to receive(:puts).with("Your age:")
        expect(output).to receive(:puts).with("Привет, John Smith, тебе меньше 18 лет, но начать программировать никогда не поздно")
        greeting(output, input)
    end
end