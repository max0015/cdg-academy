require 'rspec'
require 'stringio'

require_relative '../csword.rb'

RSpec.describe 'csword' do

    it "outputs power of 2" do
        output = StringIO.new()

        expect(output).to receive(:puts).with(64)
        csword("lovecs", output)
    end

    it "outputs reverse word" do
        output = StringIO.new()

    	expect(output).to receive(:puts).with("evol")
        csword("love", output)
    end
end