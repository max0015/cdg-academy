def csword(word, output=STDOUT)
	if word.downcase.end_with?("cs")
		output.puts 2 ** word.size
	else
		output.puts word.reverse
	end
end

if __FILE__ == $0
	csword(ARGV[0])
end