def get_pokemon(input=STDIN, output=STDOUT)
	output.puts("Enter pokemon`s name:")
	name = input.gets.chomp
	output.puts("Enter pokemon`s color:")
	color = input.gets.chomp

	return {name: name, color: color}
end

def pokemons(input=STDIN, output=STDOUT)
	list = []

	output.puts("How many pokemons you wish to add?")
	count = input.gets.to_i

	count.times do
		list << get_pokemon(input, output)
	end

	list

end

if __FILE__ == $0
	puts pokemons.inspect
end